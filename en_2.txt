﻿$ writen by hand by AlainProvist
lang "English"
set 2
$ Important note : some strings have space and tabs (generally for the info text boxes), it will be up to you to put
$ the correct number of tabs to get all the content aligned after the " : ".
0x00000001 "Processing..."
0x00000002 "Keywords"
0x00000003 "Src Channel"
0x00000004 "Dest Channel"
0x00000005 "Plugin name"
0x00000006 "Plugin state"
0x00000008 "Started"
0x00000009 "Stopped"
0x0000000A "Name : 		"
0x0000000B "Author : 	"
0x0000000C "Version : 	"
0x0000000D "Description : "
0x0000000E "Item name"
0x0000000F "ID : 		"
0x00000010 "Address : 	"
0x00000011 "Color : 		"
0x00000012 "BodyPart : 	"
0x00000013 "Category : 	"
0x00000014 "Level : 		"
0x00000015 "Price : 		"
0x00000016 "Flags : 		"
0x00000017 "Skill name"
0x00000018 "Class restr. : 	"
0x00000019 "Cooldown : 	"
0x0000001A "Range : 	"
0x0000001B "AOE radius : 	"
$ U for Use, S for Sell, K for Keep, D for discard
0x0000001C "U"
0x0000001D "S"
0x0000001E "D"
0x0000001F "K"
0x00000020 "Profile name"
0x00000021 "Profile renamed. 
Do you want to delete the previous file %s.akss ?"
0x00000022 "No"
0x00000023 "Yes"
0x00000024 "Seller profile %s has been changed. 
Do you want to save it ?"
0x00000025 "The file %s.akss already exists. 
Do you want to replace it ?"
0x00000026 "The file %s.akss has been restored."
0x00000027 "Ok"
0x00000028 "Profile renamed. 
Do you want to delete the previous file %s.akcs ?"
0x00000029 "Combat profile %s has been changed. 
Do you want to save it ?"
0x0000002A "The file %s.akcs already exists. 
Do you want to replace it ?"
0x0000002B "The file %s.akcs has been restored."
0x0000002C "Do you want to delete the file %s.akcs ?"
0x0000002D "Do you want to delete the file %s.akss ?"
0x0000002E "readme.txt is missing !"
0x0000002F "Never"
0x00000030 "Always"
0x00000031 "Auto"
0x00000032 "Items to discard"
0x00000033 "Items to use"
0x00000034 "Items to sell"
0x00000035 "Items to keep"
0x00000036 "All items"
0x00000037 "Inventory items"
0x00000038 "Drops items"
0x00000039 "Title : 		"
0x0000003A "Guild : 		"
0x0000003B "Type : 		"
0x0000003C "TypeID : 	"
0x0000003D "Manual"
0x0000003E "Auto Slot 1"
0x0000003F "Auto Slot 2"
0x00000040 "Auto Slot 3"
0x00000041 "Auto Slot 4"
0x00000042 "TemplateID : 	"